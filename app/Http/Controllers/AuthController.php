<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class AuthController extends Controller
{
    public function bio(){
        return view('form');
    }

    public function kirim(Request $request){
        //dd($request->all());
        $namad = $request['namadpn'];
        $namab = $request['namablk'];
        return view('selamatdatang', compact('namad','namab'));
    }
}
